#include <string>
//#include <vector>
#include <sstream>
#include <iostream>
#include <iterator>
#include <fstream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
//#include "wingetopt.h"
#include <iomanip>
#include <queue>
#include "externstuff.h"
#include <list>
#include <cstring>


using namespace std;







class cla
{

public:

    cla(int argc, char *argv[])
    {

        int c;
        opterr = 0;
//        char *temp;
        while ((c = getopt(argc, argv, "s:l")) != -1)
            switch (c)
            {
            case 's':
                aflag = 1;
                algo = optarg;
                break;
            case 'l':
                lflag = 1;
                break;
            case '?':
                if (optopt == 'c')
                    fprintf(stderr, "Option -%c requires an argument.\n", optopt);
                else if (isprint(optopt))
                    fprintf(stderr, "Unknown option `-%c'.\n", optopt);
                else
                    fprintf(stderr,
                        "Unknown option character `\\x%x'.\n",
                        optopt);
            default:
                abort();
            }




        input = argv[optind];


    }





};

class instrs
{
    int index;
    int last;
    int intrnum;


    void readit(string filename)
    {
        string line;
        ifstream myfile(filename.c_str());
        if (myfile.is_open())
        {
            while (getline(myfile, line))
            {
                tokenizer(line);
                
            }
            myfile.close();
        }

        else cout << "Unable to open file";


    }

public:
    vector <Int2> instructions;

    instrs(string filename)
    {
        index = 0;
        last = 0;
        intrnum = 0;
        readit(filename);


    }



    bool someLeft()
    {
        if (instructions.size() > index)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    int getLast()
    {
        return last;
    }

    int viewFirst()
    {
        Int2 temp = instructions.at(0);
        int first = temp.a[1];
        return first;

    }

    Int2 getInstr()
    {
        if (instructions.size() > index)

        {
            Int2 temp = instructions.at(index);
            last = index;
            if (lflag == 1) cout << temp.a[0] << ":     " << temp.a[2] << " add " << temp.a[1] << endl;
            index++;
            
            return temp;
        }
    }

    vector<Int2> getInstr(int time)
    {
        vector<Int2> a;
        if (instructions.size() > index)
        {
            Int2 temp;
            
            for (int i = index; index < instructions.size(); i++)
            { 
                temp = instructions.at(i);
                if (temp.a[0] <= time)
                {
                    a.push_back(temp);
                    if (lflag == 1) cout << temp.a[0] << ":     " << temp.a[2] << " add " << temp.a[1] << endl;
                    index++;
                }
                else if (temp.a[0] > time)
                {
                     break;
                }
            
            }
            
        }

        return a;
    }


    void tokenizer(string line)
    {
        stringstream os(line);
        string temp1;
        string temp2;

        os >> temp1;
        os >> temp2;
        Int2 temp;
        temp.a[0] = atoi(temp1.c_str());
        temp.a[1] = atoi(temp2.c_str());
        temp.a[2] = intrnum;
        if (temp1.at(0) == '#')
        {

        }
        else
        {
            intrnum++;
            instructions.push_back(temp);
        }
    }

};

class IOSCHED 
{

public:
    instrs *instructions;
    vector<Int2> inTime;
    bool schedulerOver;
    int lastPos;
    Int2 running;
    int queuenum;

    IOSCHED(instrs &abs)
    {
        instructions = &abs;
        lastPos = 0;
        queuenum = 1;
    }

    void run()
    {
        Int2 first = instructions->getInstr();
        first.a[5] = 1;
        inTime.push_back(first);
        curr_time = inTime.front().a[0];
        queuenum = 2;
        while (instructions->someLeft() || inTime.size() > 0)
        {
            vector<Int2> temp;
            if (inTime.size() == 0) 
            {
                first = instructions->getInstr();
                first.a[5] = queuenum;
                inTime.push_back(first);
                curr_time = inTime.front().a[0];
                next_time = curr_time;
            }
            if (inTime.size() != 0)
            {
                nextIO(); 
            }
           
            if (instructions->someLeft())
            {
                temp = instructions->getInstr(next_time);
                for (int i = 0; i < temp.size(); i++)//make sure it works when the vector is empty too
                {
                    temp.at(i).a[5] = (queuenum == 1) ? 2 : 1;
                }
                inTime.reserve(temp.size() + inTime.size());
                inTime.insert(inTime.end(), temp.begin(), temp.end());
            }
            curr_time = next_time;
            
            if (next_time > total_time) total_time = next_time;
            if (lflag == 1) cout << total_time << ":     " << running.a[2] << " finish " << curr_time - running.a[0]  << endl;
            running.a[4] = curr_time;
            finished.push_back(running);
            checkqueue();
        }
    }

public:

    void run(Int2 instr) 
    {
        
        if(instr.a[0] != 1) curr_time = next_time;
        else next_time = curr_time;

        if (lflag == 1) cout << curr_time << ":     " << instr.a[2] << " issue " << instr.a[1] << " " << lastPos << endl;
        instr.a[3] = curr_time;
        
        running = instr;
        if (lastPos < instr.a[1])
        {
            next_time += instr.a[1] - lastPos;
            tot_movement += instr.a[1] - lastPos;
        }
        else
        {
            next_time += lastPos - instr.a[1];
            tot_movement += lastPos - instr.a[1];
        }
        
        
        lastPos = instr.a[1];

    }

    virtual void nextIO() {};
    virtual void nextIO1() {};
    virtual void nextIO2() {};
    virtual void addtoqueue() {};
    virtual void checkqueue() {};//to decide to which queue to add to we will put the first one in one queue the second and all the others in the second queue then from there on to the third this will be decided with 
};

class SSTF : public IOSCHED
{
public:
    SSTF(instrs &instrs) : IOSCHED(instrs)
    {


    }



    void nextIO()
    {
        Int2 next;
        int shortesti;

        if (inTime.size() == 1)
        {
            next = inTime.front();
            inTime.erase(inTime.begin());
            run(next);
            return;
        }
        else {
            int currentshortest = 9999;
        for (int i = 0; i < inTime.size(); i++)
        {
            int contendershortest;
            if (inTime.at(i).a[1] > running.a[1])
            {
                contendershortest = inTime.at(i).a[1] - running.a[1];
            }
            else
            {
                contendershortest = running.a[1] - inTime.at(i).a[1];
            }
            if (contendershortest < currentshortest)
            {
                currentshortest = contendershortest;
                shortesti = i;
            }

        }
        }
        next = inTime.at(shortesti);
        inTime.erase(inTime.begin() + shortesti);
        run(next);
    }


};

class FIFO : public IOSCHED
{
public:
    FIFO(instrs &instrs) : IOSCHED(instrs)
    {
        

    }
    
    
  
    void nextIO()
    {
        Int2 next;
        next = inTime.front();
        inTime.erase(inTime.begin());
        run(next);
    }


};

class CSCAN : public IOSCHED
{
private:
    bool smallerfound;

public:
    CSCAN(instrs &instrs) : IOSCHED(instrs)
    {


    }



    void nextIO()
    {
        smallerfound = false;
        Int2 contender = {0,9999,0,0,0};
        Int2 currchamp = { 0,9999,0,0,0};
        int currchampnum = 0;
        for (int i = 0; i < inTime.size(); i++)
        {
            if (inTime.at(i).a[1] >= running.a[1])
            {
                smallerfound = true;
                contender = inTime.at(i);
                if (contender.a[1] < currchamp.a[1])
                {
                    currchamp = contender;
                    currchampnum = i;
                }
            }
            

        }
        
        if (smallerfound == false)
        {
            currchamp = inTime.at(0);
            currchampnum = 0;
            for (int i = 1; i < inTime.size(); i++)
            {
                if (currchamp.a[1] > inTime.at(i).a[1])
                {
                    currchamp = inTime.at(i);
                    currchampnum = i;
                }
            }
        }
        inTime.erase(inTime.begin() + currchampnum);
        run(currchamp);
    }


};

class SCAN : public IOSCHED
{
private:
    bool directiondown;
    bool largerfound;
    bool smallerfound;

public:
    SCAN(instrs &instrs) : IOSCHED(instrs)
    {
        directiondown = false;
        largerfound = false;
    }



    void nextIO()
    {
        Int2 currchamp;
        Int2 contender;
        int currchampnum = 0;
        if (inTime.size() == 1)
        {
            currchamp = inTime.front();
            inTime.erase(inTime.begin() + 0);
            run(currchamp);
            return;
        }
        else
        {

            
            /* //find the next biggest if found return that one if not found  directiondown=true;

            directiondown = true;
            //find the next smallest if found return that one if none found directiondown=false*/
            largerfound == false;
            if (directiondown == false)
            {
                contender.a[0] = 0; contender.a[1] = 0; contender.a[2] = 0; contender.a[3] = 0; contender.a[4] = 0;
                currchamp.a[0] = 0; currchamp.a[1] = 9999; currchamp.a[2] = 0; currchamp.a[3] = 0; currchamp.a[4] = 0;
            
                for (int i = 0; i < inTime.size(); i++)
                {
                    if (inTime.at(i).a[1] >= running.a[1])
                    {
                        largerfound = true;
                        contender = inTime.at(i);
                        if (contender.a[1] < currchamp.a[1])
                        {
                            currchamp = contender;
                            currchampnum = i;
                        }
                    }


                }
                if (largerfound == false)
                {
                    directiondown = true;

                }
                largerfound = false;
            }
            if (directiondown == true)
            {
                contender.a[0] = 0; contender.a[1] = 9999; contender.a[2] = 0; contender.a[3] = 0; contender.a[4] = 0;
                currchamp.a[0] = 0; currchamp.a[1] = 0; currchamp.a[2] = 0; currchamp.a[3] = 0; currchamp.a[4] = 0;
                currchampnum = 0;
                smallerfound = false;
                for (int i = 0; i < inTime.size(); i++)
                {
                    if (inTime.at(i).a[1] <= running.a[1])
                    {
                        smallerfound = true;
                        contender = inTime.at(i);
                        if (contender.a[1] > currchamp.a[1])
                        {
                            currchamp = contender;
                            currchampnum = i;
                        }
                    }


                }
                if (smallerfound == false)
                {
                    directiondown = false;
                    
                    nextIO();
                    return;

                }
                
            }
            inTime.erase(inTime.begin() + currchampnum);
            run(currchamp);
        }

}









};

class FSCAN : public IOSCHED
{
private:
    bool directiondown;
    bool largerfound;
    bool smallerfound;

public:
    FSCAN(instrs &instrs) : IOSCHED(instrs)
    {
        directiondown = false;
        largerfound = false;
        queuenum = 1;
    }



    void nextIO()
    {
        Int2 currchamp;
        Int2 contender;
        int currchampnum = 0;
        if (inTime.size() == 1)
        {
            currchamp = inTime.front();
            inTime.erase(inTime.begin() + 0);
            run(currchamp);
            return;
        }
        else
        {


            /* //find the next biggest if found return that one if not found  directiondown=true;

            directiondown = true;
            //find the next smallest if found return that one if none found directiondown=false*/
            largerfound == false;  
            if (directiondown == false)
            {
                contender.a[0] = 0; contender.a[1] = 0; contender.a[2] = 0; contender.a[3] = 0; contender.a[4] = 0;
                currchamp.a[0] = 0; currchamp.a[1] = 9999; currchamp.a[2] = 0; currchamp.a[3] = 0; currchamp.a[4] = 0;

                for (int i = 0; i < inTime.size(); i++)
                {
                    if (inTime.at(i).a[1] >= running.a[1] && inTime.at(i).a[5] == queuenum)
                    {
                        largerfound = true;
                        contender = inTime.at(i);
                        if (contender.a[1] < currchamp.a[1])
                        {
                            currchamp = contender;
                            currchampnum = i;
                        }
                    }


                }
                if (largerfound == false)
                {
                    directiondown = true;

                }
                largerfound = false;
            }
            if (directiondown == true)
            {
                contender.a[0] = 0; contender.a[1] = 9999; contender.a[2] = 0; contender.a[3] = 0; contender.a[4] = 0;
                currchamp.a[0] = 0; currchamp.a[1] = 0; currchamp.a[2] = 0; currchamp.a[3] = 0; currchamp.a[4] = 0;
                currchampnum = 0;
                smallerfound = false;
                for (int i = 0; i < inTime.size(); i++)
                {
                    if (inTime.at(i).a[1] <= running.a[1] && inTime.at(i).a[5] == queuenum)
                    {
                        smallerfound = true;
                        contender = inTime.at(i);
                        if (contender.a[1] > currchamp.a[1])
                        {
                            currchamp = contender;
                            currchampnum = i;
                        }
                    }


                }
                if (smallerfound == false)
                {
                    directiondown = false;

                    nextIO();
                    return;

                }

            }
            inTime.erase(inTime.begin() + currchampnum);
            run(currchamp);
        }

    }


    void checkqueue()
    {
        bool stuffleft = false;
        for (int i = 0; i < inTime.size(); i++)
        {
            if (inTime.at(i).a[5] == queuenum)
            {
                stuffleft = true;
            }
        }
        if (stuffleft == false)
        {
            (queuenum == 1)? queuenum = 2: queuenum = 1;
            directiondown = false;
            largerfound = false;

        }
    }






};


std::string input;
int lflag = 0;
int aflag = 0;
char *algo;


int total_time = 0;
int tot_movement = 0;
double avg_turnaround = 0;
double avg_waittime = 0;
int max_waittime = 0;
int curr_time = 0; 
int next_time = 0;
int IOrequestnum = -1;
vector<Int2> finished;

int main(int argc, char **argv)
{
    IOSCHED *sched;
    cla(argc, argv);

    instrs instrs(input);
    
    
   switch (*algo)
    {
    case 'i':
        sched = new FIFO(instrs);
        break;
    case 'f':
        sched = new FSCAN(instrs);
        break;
    case 'c':
        sched = new CSCAN(instrs);
        break;
   case 's':
        sched = new SCAN(instrs);
        break;
   case 'j':
        sched = new SSTF(instrs);
        break;
    default:
        sched = new FIFO(instrs);
        break;

    }
   if (lflag == 1) cout << "TRACE" << endl;

 
       sched->run();

   if (lflag == 1) 
   {
           cout << "IOREQS INFO" << endl;
           for (int i = 0; i < finished.size(); i++)
           {
               cout << "    " << finished.at(i).a[2] << ":     " << finished.at(i).a[0] << "     " << finished.at(i).a[3] << "   " << finished.at(i).a[4] << endl;
           }
   }

   for (int i = 0; i < finished.size(); i++)
   {
       avg_turnaround += finished.at(i).a[4] - finished.at(i).a[0];
       avg_waittime += finished.at(i).a[3] - finished.at(i).a[0];
       if(max_waittime < (finished.at(i).a[3] - finished.at(i).a[0]))  max_waittime = finished.at(i).a[3] - finished.at(i).a[0];
   }

   avg_turnaround = avg_turnaround / finished.size();
   avg_waittime = avg_waittime / finished.size();
   printf("SUM: %d %d %.2lf %.2lf %d\n",
       total_time,
       tot_movement,
       avg_turnaround,
       avg_waittime,
       max_waittime);

}

