SOURCES = iosched.cpp
OBJECTS = 
HEADERS = externstuff.h
EXEBIN  = iosched

all: $(EXEBIN)

$(EXEBIN) : $(OBJECTS) $(HEADERS)
	g++49 $(SOURCES) -std=gnu++11 -static-libstdc++ -o $(EXEBIN)

clean:
	rm -f $(EXEBIN) $(OBJECTS)