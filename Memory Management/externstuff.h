//#include "azar.h"
#include "pagetableentry.h"

extern std::string input;
extern std::string seeds;
extern int aflag;
extern char *algo;

extern int oflag;
extern char *options;

extern int fflag;
extern int num_frames;

//extern class azar *rlist;
#define num_pages 64

extern PTE PageTable[];
extern int *FrameTable;//physical memory frames

//extern class Instructions *instrs;

extern int unmapnums;
extern int mapnums;
extern int zeronums;
extern int pageinnums;
extern int pageoutnums;
extern int readnums;
extern int writenums;
extern int instrsnum;
extern int hand;
