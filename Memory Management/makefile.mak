SOURCES = mmu.cpp
OBJECTS = 
HEADERS = externstuff.h pagetableentry.h
EXEBIN  = mmu

all: $(EXEBIN)

$(EXEBIN) : $(OBJECTS) $(HEADERS)
	g++49 $(SOURCES) -std=gnu++11 -static-libstdc++ -o $(EXEBIN)

clean:
	rm -f $(EXEBIN) $(OBJECTS)