#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <iterator>
#include <fstream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
//#include "wingetopt.h"
#include <iomanip>
#include <queue>
#include "externstuff.h"
#include <list>
#include <cstring> 

using namespace std;


struct Int2 {
    int a[2];
};

class PTEAccess {
public:

    unsigned int GetPRESENT(PTE& PTE)
    { 
        return PTE.PRESENT; 
    }

    unsigned int GetMODIFIED(PTE& PTE)
    { 
        return PTE.MODIFIED; 
    }

    unsigned int GetREFERENCED(PTE& PTE)
    { 
        return PTE.REFERENCED;
    }
    
    unsigned int GetPAGEDOUT(PTE& PTE)
    { 
        return PTE.PAGEDOUT;
    }

    unsigned int GetPHYSICALFRAME(PTE& PTE)
    {
        return PTE.PHYSICALFRAME;
    }

    void SetREFERENCED(PTE& PTE) 
    { 
        PTE.REFERENCED = 1;
    }

    void SetMODIFIED(PTE& PTE) 
    { 
        PTE.MODIFIED = 1;
    }

    void SetPRESENT(PTE& PTE)
    { 
        PTE.PRESENT = 1;
    }

    void SetPAGEDOUT(PTE& PTE) 
    { 
        PTE.PAGEDOUT = 1; 
    }
    
    void SetPHYSICALFRAME(PTE& PTE, int frameNumber)
    {
        PTE.PHYSICALFRAME = frameNumber;
    }


    void UnsetREFERENCED(PTE& PTE)
    { 
        PTE.REFERENCED = 0; 
    }

    void UnsetMODIFIED(PTE& PTE)
    { 
        PTE.MODIFIED = 0; 
    }

    void UnsetPRESENT(PTE& PTE)
    { 
        PTE.PRESENT = 0; 
    }
    void UnsetPAGEDOUT(PTE& PTE)
    { 
        PTE.PAGEDOUT = 0;
    }

    void UnsetAll(PTE& PTE)
    {
        PTE.MODIFIED = 0;
        PTE.PRESENT = 0;
        PTE.PAGEDOUT = 0;
        PTE.PHYSICALFRAME = 100;
        PTE.PRESENT = 0;
        PTE.REFERENCED = 0;
    }

};


class cla
{

public:

    cla(int argc, char *argv[])
    {
               
        int c;
        opterr = 0;
        char *temp;
        while ((c = getopt(argc, argv, "a:o:f:")) != -1)
            switch (c)
            {
            case 'a':
                aflag = 1;
                algo = optarg;
                break;
            case 'o':
                oflag = 1;
                options = optarg;
                break;
            case 'f':
                fflag = 1;
                temp = optarg;
                num_frames = atoi(&temp[0]);
                break;

            case '?':
                if (optopt == 'c')
                    fprintf(stderr, "Option -%c requires an argument.\n", optopt);
                else if (isprint(optopt))
                    fprintf(stderr, "Unknown option `-%c'.\n", optopt);
                else
                    fprintf(stderr,
                        "Unknown option character `\\x%x'.\n",
                        optopt);
            default:
                abort();
            }




        input = argv[optind];
        seeds = argv[optind + 1];


    }





};

class instrs
{
    int index;
    int last;



     void readit(string filename)
    {
    string line;
    ifstream myfile(filename.c_str());
    if (myfile.is_open())
    {
        while (getline(myfile, line))
        {
            tokenizer(line);
        }
        myfile.close();
    }

    else cout << "Unable to open file";


    }
    
public:
    vector <Int2> instructions;

     instrs(string filename)
    {
        index = 0;
        last = 0;
        readit(filename);


    }

   
    
    bool someLeft()
    {
        if (instructions.size() > index)
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }

    int getLast()
    {
        return last;
    }

    int viewFirst()
    {
        Int2 temp = instructions.at(0);
        int first = temp.a[1];
        return first;

    }
    
    Int2 getInstr()
    {
        
        if (instructions.size() > index)

        {
            Int2 temp = instructions.at(index);
            last = index;
            index++;
            return temp;
        }
        /*else
        {
            return{ -1, -1 };
        }*/
    }


void tokenizer(string line)
{
    stringstream os(line);
    string temp1;
    string temp2;

    os >> temp1;
    os >> temp2;
    Int2 temp;
    temp.a[0] =  atoi(temp1.c_str());
    temp.a[1] = atoi(temp2.c_str());
    if (temp1.at(0) == '#')
    {
        
    }
    else
    {      
        instructions.push_back(temp);
    }
}

};


void addtoFrameTable(int page)
{
    for (int i = 0; i < num_frames; i++)
    {
        if (*(FrameTable + i) == -1)
        {
            *(FrameTable + i) = page;
            break;
        }

    }

}

int* MakeFrameTable(int size)
{
    int *tempPointer;
    
    tempPointer = new int[size];
    for (int i = 0; i < size; i++)
    {
        *(tempPointer + i) = -1;
    }

    
    return tempPointer;
}

class azar
{
    int currentindex;
    int size;
    std::vector<int> randomizer;
    
    //int next();

public:

    azar(string filename)
{
    
    
    currentindex = -1;
    readandstore(filename);
    

}



int next()
{
    int nextnum;
    if (currentindex < size)//????
    {       
        nextnum = randomizer.at(currentindex);
        //cout << "random number" << currentindex << endl; // random number finder
        currentindex++;
    }
    else
    {
        nextnum = randomizer.at(0);
        currentindex = 1;
    }
    return nextnum;
}

int myrandom(int burst)
{
    int a = next();
    //cout << " seed " << a <<  endl;
    return (a % burst);
    //return 1 + (next() % burst);

}

void readandstore(string filename)
{
    
    string line;
    ifstream myfile(filename.c_str());
    if (myfile.is_open())
    {
        while (getline(myfile, line))
        {
            tokenizer(line);
        }
        myfile.close();
    }

    else cout << "Unable to open file";

    currentindex = 0;
}


void tokenizer(string line)
{
    stringstream os(line);
    string temp;

    while (os >> temp)
    {

        
        if (currentindex == -1)
        {
            randomizer.reserve(atoi(temp.c_str()));
            size = atoi(temp.c_str());
            currentindex++;
        }
        else
        {
            randomizer.push_back(atoi(temp.c_str()));
            currentindex++;
        }
    }


}
};

class MMU
{
protected:    
    int instruction;
    int pageNum;
    int indexFrameTable;
    int indexPageTable;
    int O;
    int p;
    int f;

    PTEAccess access;
    vector <int> onesinlist;

public:
        
    MMU()
    {
        instruction = -1;
    
        indexFrameTable = 0;
        indexPageTable = 0;
        O = 0;
        p = 0;
        f = 0;



        for (int i = 0; i < strlen(options); i++) 
        {
            if (*(options + i) == 'O') O = 1;
            if (*(options + i) == 'p') p = 1;
            if (*(options + i) == 'f') f = 1;
        }

    }
    void populate(Int2 instr)
    {
        instruction++;
        int readorwrite = instr.a[0];
        indexPageTable = instr.a[1];
        PTE *temp;
        pageNum = indexPageTable;
        
        if(*algo == 'l') updatelist(indexPageTable);//LRU

        if (O == 1)
        {
            cout << "==> inst: " << readorwrite << " " << indexPageTable << endl;
        }
        if (indexFrameTable < num_frames && !inFrameTable(instr.a[1]) && spaceinFrameTable())//new frame coming in with space in FRAMETABLE
        {
            temp = new PTE;
            access.UnsetAll(*temp);
            switch (readorwrite)
            {
            case 0:
                access.SetREFERENCED(*temp);
                readnums++;
                break;
            case 1:
                access.SetMODIFIED(*temp);
                access.SetREFERENCED(PageTable[pageNum]);
                writenums++;
                break;
            }

            access.SetREFERENCED(*temp);
            access.SetPRESENT(*temp);
            access.SetPHYSICALFRAME(*temp, instr.a[1]);
            onesinlist.push_back(indexPageTable);

            PageTable[indexPageTable] = *temp;

            addtoFrameTable(indexPageTable);
            //*(FrameTable + indexFrameTable) = indexPageTable;

            if (O == 1)
            {
                cout << instrsnum << ": ZERO " << setfill(' ') << setw(8) << indexFrameTable << endl;
                cout << instrsnum << ": MAP  " << setfill(' ') << setw(4) << access.GetPHYSICALFRAME(*temp) << setw(4) << indexFrameTable << endl;

            }
            if (*algo == 'f' || *algo == 's' || *algo == 'c' || *algo == 'a' || *algo == 'Y') addq(access.GetPHYSICALFRAME(*temp));
            zeronums++;
            mapnums++;
            indexFrameTable++;
        }
        else if (inFrameTable(indexPageTable))//FRAME EXISTING IN MEMORY AKA FRAMETABLE is going to be modified
        { 
            
            switch (readorwrite)
            {
            case 0:
                access.SetREFERENCED(PageTable[pageNum]);
                readnums++;
                break;
            case 1:
                access.SetMODIFIED(PageTable[pageNum]);
                access.SetREFERENCED(PageTable[pageNum]);
                writenums++;
                break;
            }
            
            

        }
        else if(!inFrameTable(indexPageTable) && !inPageTable(indexPageTable) && !spaceinFrameTable())
        {
            int pageoutNum = unmapnum();
            temp = new PTE;
            access.UnsetAll(*temp);
            switch (readorwrite)
            {
            case 0:
                access.SetREFERENCED(*temp);
                readnums++;
                break;
            case 1:
                access.SetMODIFIED(*temp);
                access.SetREFERENCED(PageTable[pageNum]);
                writenums++;
                break;
            }

            access.SetREFERENCED(*temp);
            access.SetPRESENT(*temp);
            access.SetPHYSICALFRAME(*temp, instr.a[1]);

            indexFrameTable = unmap(pageoutNum, indexPageTable);//takes out the page from the vector and puts in new one takes the page out from the frametable and puts the new one  
            

            PageTable[indexPageTable] = *temp;
            *(FrameTable + indexFrameTable) = indexPageTable;

            if (O == 1)
            {
                cout << instrsnum << ": ZERO " << setfill(' ') << setw(8) << indexFrameTable << endl;
                cout << instrsnum << ": MAP  " << setfill(' ') << setw(4) << access.GetPHYSICALFRAME(*temp) << setw(4) << indexFrameTable << endl;

            }
            if (*algo == 'f' || *algo == 's' || *algo == 'c' || *algo == 'a' || *algo == 'Y') addq(access.GetPHYSICALFRAME(*temp));
            mapnums++;
            zeronums++;
            indexFrameTable++;

        }
        else if (!inFrameTable(indexPageTable) && inPageTable(indexPageTable) && !spaceinFrameTable())
        {
            int pageoutNum = unmapnum();
            temp = &PageTable[indexPageTable];
            switch (readorwrite)
            {
            case 0:
                access.SetREFERENCED(*temp);
                readnums++;
                break;
            case 1:
                access.SetMODIFIED(*temp);
                access.SetREFERENCED(PageTable[pageNum]);
                writenums++;
                break;
            }
            
            access.SetREFERENCED(*temp);
            access.SetPRESENT(*temp);
            access.SetPHYSICALFRAME(*temp, instr.a[1]);

            indexFrameTable = unmap(pageoutNum, indexPageTable);//takes out the page from the vector and puts in new one takes the page out from the frametable and puts the new one  


            PageTable[indexPageTable] = *temp;
            *(FrameTable + indexFrameTable) = indexPageTable;

            (access.GetPAGEDOUT(*temp) == 1) ? pageinnums++ : zeronums++;
            
            if (O == 1)
            {
                if (access.GetPAGEDOUT(*temp) == 1)
                {
                    
                    //access.UnsetPAGEDOUT(*temp);
                    cout << instrsnum << ": IN   " << setfill(' ') << setw(4) << access.GetPHYSICALFRAME(*temp) << setw(4) << indexFrameTable << endl;
                }
                else
                {
                    cout << instrsnum << ": ZERO " << setfill(' ') << setw(8) << indexFrameTable << endl;
                }
                
                cout << instrsnum << ": MAP  " << setfill(' ') << setw(4) << access.GetPHYSICALFRAME(*temp) << setw(4) << indexFrameTable << endl;

            }

            if (*algo == 'f' || *algo == 's' || *algo == 'c' || *algo == 'a' || *algo == 'Y') addq(access.GetPHYSICALFRAME(*temp));

            mapnums++;
            indexFrameTable++;

        }
        if (p == 1) printp();
        if (f == 1) printf();
        
    }

    void printf()
    {
        PTEAccess access;
        for (int i = 0; i < num_frames; i++) {
            (*(FrameTable + i) == -1) ? cout << "* " : cout << *(FrameTable + i) << " ";
        } 
        if (*algo == 'X' || *algo == 'c')
        {
            cout << " || hand = " << hand;
        }
        
        
        cout << endl;
    }

    void printp()
    {
        PTEAccess access;
        for (int i = 0; i < num_pages; i++)
        {
            PTE *temp = &PageTable[i];
            if (access.GetPRESENT(*temp) == 1)
            {
                cout << i << ":";
                (access.GetREFERENCED(*temp) == 1) ? cout << "R" : cout << "-";
                (access.GetMODIFIED(*temp) == 1) ? cout << "M" : cout << "-";
                (access.GetPAGEDOUT(*temp) == 1) ? cout << "S " : cout << "- ";
            }
            else
            {
                (access.GetPAGEDOUT(*temp) == 1) ? cout << "# " : cout << "* ";
            }

        }

        cout << endl;
    }

    bool inList(int instrucion)
    {
        for (std::vector<int>::size_type i = 0; i != onesinlist.size(); i++) {
            int temp = onesinlist.at(i);
            if (instrucion == temp)
            {
                return true;
            }
        }
        return false;
    }

    
    bool inFrameTable(int instrucion)
    {
        for (unsigned int a = 0; a < num_frames; a = a + 1) {
            int temp = *(FrameTable + a);
            if (instrucion == temp)
            {
                return true;
            }
        }
        return false;
    }

    bool inPageTable(int instrucion)
    {
        if (access.GetPHYSICALFRAME(PageTable[instrucion]) == instrucion)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
        
    bool spaceinFrameTable()
    {
        for (unsigned int a = 0; a < num_frames; a = a + 1) {
            int temp = *(FrameTable + a);
            if (-1 == temp)
            {
                return true;
            }
        }
        return false;
    }

    void pageout(int numout, int framenum)
    {
        PTE *temp = &PageTable[numout];

        int out = access.GetMODIFIED(*temp);
        if (out == 1) pageoutnums++;
        if (out == 1) {
            access.SetPAGEDOUT(*temp);
            access.UnsetMODIFIED(*temp);
            if (O == 1)
            {
                cout << instruction << ": OUT  " << setfill(' ') << setw(4) << numout << setw(4) << framenum << endl;
                
            }
        }

    }


    int unmap(int numtopageout, int pagein)
    {
        int framenum;
        for (std::vector<int>::size_type i = 0; i != onesinlist.size(); i++) {
            int temp = onesinlist.at(i);
                if (numtopageout == temp)
                {
                    onesinlist.at(i) = pagein;
                    
                    break;
                }
        }

        for (int i = 0; i < num_frames; i++)
        {
            if (*(FrameTable + i) == numtopageout)
            {
                int numout = *(FrameTable + i);
                *(FrameTable + i) = pagein;
                framenum = i;
                access.UnsetPRESENT(PageTable[numtopageout]);
                if (O == 1)
                {
                    cout << instruction << ": UNMAP" << setfill(' ') << setw(4) << numtopageout << setw(4) << framenum << endl;
                    
                }
                unmapnums++;
                PTE *temp = &PageTable[numout];
                pageout(numout,framenum);
                
                break;
            }
        }
        
        return framenum;
       
    }




    virtual int unmapnum() { return -1; }
    //virtual void updatelist(int entry) = 0;
    virtual void updatelist(int entry) {};
    virtual void addq(int page) {};


};




void zeroallTable()
{
    PTEAccess access;
    for (int i = 0; i < num_pages; i++)
    {
        PTE *temp = &PageTable[i];
        access.UnsetMODIFIED(*temp);
        access.UnsetPAGEDOUT(*temp);
        access.UnsetPRESENT(*temp);
        access.UnsetREFERENCED(*temp);
        access.SetPHYSICALFRAME(*temp, 100);
    }
}

int numofPagetable()//number of pages in page table
{
    int pagecount = 0;
    PTEAccess access;
    for (int i = 0; i < num_pages; i++)
    {
        PTE *temp = &PageTable[i];
        if (access.GetPHYSICALFRAME(*temp) == 100)
        {
            pagecount++;
        }
    }

    return num_pages - pagecount;
}



void printend()
{
    int P = 0;
    int F = 0;
    int S = 0;

    for (int i = 0; i < strlen(options); i++)
    {
        if (*(options + i) == 'P') P = 1;
        if (*(options + i) == 'F') F = 1;
        if (*(options + i) == 'S') S = 1;
    }
    PTEAccess access;
    int totalpages = numofPagetable();
    if (P == 1)
    {
        for (int i = 0; i < num_pages; i++)
        {
            PTE *temp = &PageTable[i];
            if (access.GetPRESENT(*temp) == 1)
            {
                cout << i << ":";
                (access.GetREFERENCED(*temp) == 1) ? cout << "R" : cout << "-";
                (access.GetMODIFIED(*temp) == 1) ? cout << "M" : cout << "-";
                (access.GetPAGEDOUT(*temp) == 1) ? cout << "S " : cout << "- ";
            }
            else
            {
                (access.GetPAGEDOUT(*temp) == 1) ? cout << "# " : cout << "* ";
            }
            
        }
        cout << endl;
    }
        if (F == 1)
        {
            for (int i = 0; i < num_frames; i++) {
                (*(FrameTable + i) == -1) ? cout << "* " : cout << *(FrameTable + i) << " ";
            } cout << endl;
        }
        
        if (S == 1) 
        {
            
            cout << "SUM " << instrsnum + 1 << " U=" << unmapnums << " M=" << mapnums << " ";
            cout << " I=" << pageinnums << " O=" << pageoutnums << " Z=" << zeronums << " ===> " << ((((long long)mapnums + unmapnums) * 400) + ((pageinnums + pageoutnums) * 3000) + (zeronums * 150) + readnums + writenums) << endl;
        }
    
}

class FIFO : public MMU
{
    
    queue <int> *fifo;

public:

    FIFO()
    {
        fifo = new queue <int>;
        MMU();

    }

    int unmapnum()
    {
        int firstIn = -1;
        if (fifo->size() != 0)
        {
            firstIn = fifo->front();
            fifo->pop();

            return firstIn;
        }
        else
        {
            return -1;
        }
    }

    void addq(int page)
    {
        fifo->push(page);
    }
};


class SECONDCHANGE : public MMU
{

    queue <int> *sc;
public:
    SECONDCHANGE()
    {
        sc = new queue <int>;
        MMU();
    }

    int unmapnum()
    {
        int firstIn = -1;
        if (sc->size() != 0)
        {
            
            
            firstIn = sc->front();
            
            if (access.GetREFERENCED(PageTable[firstIn]) == 0)
            {
            
                sc->pop();

                return firstIn;
                
            }
            else
            {
                for (int i = 0; i < sc->size(); i++)
                {
                    firstIn = sc->front();
                    if (access.GetREFERENCED(PageTable[firstIn]) == 0)
                    {
                        access.UnsetREFERENCED(PageTable[firstIn]);
                        sc->pop();

                        return firstIn;
                    }
                    access.UnsetREFERENCED(PageTable[firstIn]);
                    sc->pop();
                    sc->push(firstIn);
                
                }
                firstIn = sc->front();
                sc->pop();
                return firstIn;

            }
            
            
      
        }
        else
        {
            return -1;
        }
    }

    void addq(int page)
    {
        sc->push(page);
    }

};

class clockf : public MMU
{

    int *clock;
    
    int temp;
    int index;

public:
    clockf()
    {
        
        temp = -1;
        index = 0;
        clock = new int[num_frames];
        MMU();
    }

    int unmapnum()
    {
        int out;

      

        if(temp == -1)
        {

            if (access.GetREFERENCED(PageTable[*(clock + hand)]) == 0)
            {
                out = *(clock + hand);
                
                return out;

            }
            else
            {
                
                while (access.GetREFERENCED(PageTable[*(clock + hand)]) == 1)
                {
                    access.UnsetREFERENCED(PageTable[*(clock + hand)]);
                    addhand();
                }
                out = *(clock + hand);
                index = 100;
                return out;
            }



        }
        else
        {
            out = *(clock + hand);
            *(clock + hand) = temp;
            return out;
        }
    }



    void addhand()
    {
        if (hand == num_frames - 1)
        {
            hand = 0;
        }
        else
        {
            hand++;
        }
    }

    void addq(int page)
    {
        if (index < num_frames)
        {
            *(clock + index) = page;
            index++;
        }
        else if (index == 100)
        {
            *(clock + hand) = page;
            addhand();
        }
        else
        {
            temp = page;
        }
    }

};

class clockp : public MMU
{

    
    
    
    

public:
    clockp()
    {
        MMU();
    }

    int unmapnum()
    {
        int out;
        if (access.GetPRESENT(PageTable[hand]) == 1)
        {
            if (access.GetREFERENCED(PageTable[hand]) == 0)
            {
                out = access.GetPHYSICALFRAME(PageTable[hand]);
                hand++;
                return out;

            }
            else
            {

                while (access.GetREFERENCED(PageTable[hand]) == 1)
                {
                    access.UnsetREFERENCED(PageTable[hand]);
                    addhand();
                }
                out = access.GetPHYSICALFRAME(PageTable[hand]);
                hand++;
                return out;
            }
        }
        else
        {
            addhand();
            out = unmapnum();
            
            return out;
        }
    }



    void addhand()
    {  
        if (hand < num_pages)
        {
            hand++;
            while (access.GetPRESENT(PageTable[hand]) == 0 && hand < num_pages)
            {
                hand++;

            }
            if (hand == num_pages)
            {
                addhand();
            }
        }
        else if (hand == num_pages)
        {
            
            hand = 0;
            while (access.GetPRESENT(PageTable[hand]) == 0 && hand < num_pages)
            {
                hand++;
            }
        }
    }


};


class NRU : public MMU
{
    int pagereplacementrequest;
    azar *rlist;

public:
    
    NRU(std::string seeds)
    {
        pagereplacementrequest = 0;
        MMU();
        rlist = new azar(seeds);
    }

    int unmapnum()
    {
        
        vector<PTE> *classesvector = new vector<PTE>[4];

        int i = 0;
        while (i < num_pages)
        {
            if (access.GetPRESENT(PageTable[i]) == 1)
            {
                if (access.GetREFERENCED(PageTable[i]) == 0 && access.GetMODIFIED(PageTable[i]) == 0) classesvector[0].push_back(PageTable[i]);
                else if (access.GetREFERENCED(PageTable[i]) == 0 && access.GetMODIFIED(PageTable[i]) == 1) classesvector[1].push_back(PageTable[i]);
                else if (access.GetREFERENCED(PageTable[i]) == 1 && access.GetMODIFIED(PageTable[i]) == 0) classesvector[2].push_back(PageTable[i]);
                else if (access.GetREFERENCED(PageTable[i]) == 1 && access.GetMODIFIED(PageTable[i]) == 1) classesvector[3].push_back(PageTable[i]);
            }
            i++;
       }
        int number;
        int classnum;
        if (classesvector[0].size() > 0)
        {
            number = rlist->myrandom(classesvector[0].size());
            classnum = 0;
        }
        else if (classesvector[1].size() > 0)
        {
            number = rlist->myrandom(classesvector[1].size());
            classnum = 1;
        }
        else if (classesvector[2].size() > 0)
        {
            number = rlist->myrandom(classesvector[2].size());
            classnum = 2;
        }
        else if (classesvector[3].size() > 0)
        {
            number = rlist->myrandom(classesvector[3].size());
            classnum = 3;
        }
        pagereplacementrequest++;
        if (pagereplacementrequest == 10)
        {
            int a = 0;
            while (a < num_pages)
            {
                if (access.GetPRESENT(PageTable[a]) == 1)
                {
                    access.UnsetREFERENCED(PageTable[a]);
                }
                a++;
            }


            pagereplacementrequest = 0;
        }

        return access.GetPHYSICALFRAME(classesvector[classnum].at(number));
    
    
    
    }


};


class LRU : public MMU
{
    list<int> linkedlist;
    
    
public:    
    LRU()
    {
        MMU();
    }

    void updatelist(int entry)
    {    
        linkedlist.remove(entry);
        linkedlist.push_front(entry);

    }
    
    int unmapnum()
    {
        
            for(std::list<int>::reverse_iterator rit = linkedlist.rbegin(); rit != linkedlist.rend(); ++rit)
            {
                for (int i = 0; i < num_frames; i++)
                {
                    if (*(FrameTable + i) == *rit)
                    {
                        return *(FrameTable + i);
                    }
                }
            }
                

    }


};

class Random : public MMU
{
    azar *rlist;

public:
    Random(std::string seeds)
    {
        MMU();
        rlist = new azar(seeds);
    }

    int unmapnum()
    {
        int number = rlist->myrandom(num_frames);
        return *(FrameTable + number);

    }



};

class AGING : public MMU
{
    
    struct ager
    {
        unsigned int counter : 32;
        int page;
    };
    
    ager *list;
    int empty;
    bool pagedstart;
    

public:
    AGING()
    {
        pagedstart = false;
        empty = 0;
        MMU();
        list = new ager[num_frames];
    }

    /*void updatelist(int entry)
    {
        linkedlist.remove(entry);
        linkedlist.push_front(entry);

    }*/

    int unmapnum()
    {
        //int pagenumout;
        for (int i = 0; i < num_frames; i++)
        {
            ager a = *(list + i);
            a.counter = a.counter >> 1;
            *(list + i) = a;
            
        }
        
        
        
        PTE *temp;
        for (int i = 0; i < num_frames; i++)
        {
            ager a = *(list + i);
            temp = &PageTable[a.page];
            
            int refrenced = access.GetREFERENCED(*temp);
            if (refrenced == 1)
            {
                
                a.counter = a.counter | 0x80000000;
                access.UnsetREFERENCED(*temp);
                *(list + i) = a;
            }
        }

        ager a = *(list + 0);// is it zero or last page
        empty = 0;
     
        for (int i = 0; i < num_frames; i++)
        {
            ager b = *(list + i);
            
            if (b.counter < a.counter)
            {
                a = b;
                empty = i;
            }
        }

        return a.page;
        
    }

    void addq(int page)
    {
        if (empty < num_frames && !pagedstart)
        {
            ager temp;
            temp.counter = 0x00000000;
            temp.page = page;
            list[empty] = temp;
            empty++;
            if (empty == num_frames)
            {
                pagedstart = true;
            }
        }
        else
        {
            ager temp;
            temp.counter = 0x00000000;
            temp.page = page;
            list[empty] = temp;
        }
    }


};

class AGINGP : public MMU
{

    struct ager
    {
        unsigned int counter : 32;
        int page;
    };

    ager *list;
    int empty;
    bool pagedstart;


public:
    AGINGP()
    {
        empty = 0;
        pagedstart = false;


        MMU();
        list = new ager[num_pages];
    }



    int unmapnum()
    {
        PTE *temp;
        ager x;
        for (int i = 0; i < num_pages; i++)
        {
            
            temp = &PageTable[i];
            if (access.GetPRESENT(*temp) == 1)
            {
                x = *(list + i);
                x.counter = x.counter >> 1;
                
                if (access.GetREFERENCED(*temp) == 1)
                {
                    x.counter = x.counter | 0x80000000;
                    access.UnsetREFERENCED(*temp);
                }
                *(list + i) = x;
            }
        }



        PTE *tempc;
        int a = 0;
        tempc = &PageTable[a];
        while (access.GetPRESENT(*tempc) != 1)
        {
            tempc = &PageTable[++a];
        }
        ager champion = *(list + a);


        for (int i = 0; i < num_pages; i++)
        {
            temp = &PageTable[i];
            
            if(access.GetPRESENT(*temp) != 1)
            {
                continue;
            }          
            
            ager contender = *(list + i);

            if (contender.counter < champion.counter)
            {
                champion = contender;
            }
        }
        champion.counter = 0x00000000;
        
        
        *(list + champion.page) = champion;

        return champion.page;

    }

    void addq(int page)
    {
        ager temp;
        temp.counter = 0x00000000;
        temp.page = page;
        *(list + page) = temp;
    }


};

std::string input;
std::string seeds;
int aflag = 0;
char *algo;

int oflag = 0;
char *options;

int fflag = 0;
int num_frames = 32;//default number



PTE PageTable[num_pages] = {};
int *FrameTable;
//class Instructions *instrs;
int unmapnums = 0;
int mapnums = 0;
int zeronums = 0;
int pageinnums = 0;
int pageoutnums = 0;
int instrsnum = -1;
int readnums = 0;
int writenums = 0;
int hand = 0;

int main(int argc, char **argv) 
{
    
    MMU *pager;
    cla(argc, argv);

    instrs instrs(input);
    //instrs->readFile(input);
    zeroallTable();
    FrameTable = MakeFrameTable(num_frames);
    switch (*algo)
    {
    case 'r':
        
        pager = new Random(seeds);
        break;
    case 'N':
        pager = new NRU(seeds);
        break;
    case 'l':
        pager = new LRU();
        break;
    case 'f':
        pager = new FIFO();
        break;
    case 's':
        pager = new SECONDCHANGE();
        break;
    case 'c':
        pager = new clockf();
        break;
    case 'X':
        pager = new clockp();
        break;
    case 'a':
        pager = new AGING();
        break;
    case 'Y':
        pager = new AGINGP();
        break;
    default:
        pager = new LRU();
        break;

    }

    while (instrs.someLeft())
    {
        instrsnum++;
        pager->populate(instrs.getInstr());
        
    }

    printend();
    /*cout << "Algo: " << algo << ", Options: " << options << ",  num_frames tempchar :" << num_frames;*/
	
	
	
	
}

