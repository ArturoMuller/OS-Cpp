invocation is as follows:
./mmu [-a<algo>] [-o<options>] [�f<num_frames>] inputfile randomfile 
(optional arguments in any order)

algo:
page replacement algorithms  
Based on Physical Frames:  LRU (l), Random (r), FIFO (f), Second Chance (s),  Clock (c), Aging (c)
Based on Virtual pages: NRU (N), Clock (X), Aging (Y)

options (any number of them):
Verbose (O), Page Table (P), Frame Table (F), Summary (S), Page Table after each instruction (p), Frame Table after each instruction (f), aging after each instruction for N,X,Y (a).

